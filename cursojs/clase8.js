var nico = {
    nombre: 'Nicolas',
    apellido: 'Reyes',
    edad: 24
}

var bryan = {
    nombre: 'Bryan',
    apellido: 'Picazo',
    edad: 23
}

function imprimirNombreenMayusculas(persona) {
    var { nombre } = persona
    console.log(nombre.toUpperCase())
}

imprimirNombreenMayusculas(nico)
imprimirNombreenMayusculas(bryan)
// imprimirNombreenMayusculas( { nombre: 'Ppepito'} )

function cumpleanos(persona) {
    return {
        ...persona,
        edad: persona.edad +1
    }

}
