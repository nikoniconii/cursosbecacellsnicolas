var nico = {
    nombre: 'Nicolas',
    apellido: 'Reyes',
    edad: 24
}

var bryan = {
    nombre: 'Bryan',
    apellido: 'Picazo',
    edad: 23
}

function imprimirNombreenMayusculas({ nombre }) {
    console.log(nombre.toUpperCase())
}

imprimirNombreenMayusculas(nico)
imprimirNombreenMayusculas(bryan)
imprimirNombreenMayusculas( { nombre: 'Ppepito'} )

