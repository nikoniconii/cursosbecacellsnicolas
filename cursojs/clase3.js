var nombre = 'Nicolas', edad = 24

function imprimirEdad(n, e) {
    console.log(`${n} tiene ${e} años`)
}

imprimirEdad(nombre, edad)
imprimirEdad('Raul', 30)
imprimirEdad('Nancy', 33)
imprimirEdad('Lucas', 25)